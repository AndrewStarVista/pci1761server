#include "PCI1761Trigger.h"

extern boost::mutex mutexT;
extern vector<string> msgList;

string getSysTime()
{
	SYSTEMTIME sys;
	GetLocalTime(&sys);
	std::stringstream ss;

	stringstream os_year_, os_month_, os_day_, os_hour_, os_minute_, os_second_, os_millisecond_;
	os_year_ << sys.wYear;
	os_month_ << sys.wMonth;
	os_day_ << sys.wDay;
	os_hour_ << sys.wHour;
	os_minute_ << sys.wMinute;
	os_second_ << sys.wSecond;
	os_millisecond_ << sys.wMilliseconds;

	string stmStr;
	char buf[1024];
	sprintf(buf, "%04d%02d%02d%02d%02d%02d%03d", atoi(os_year_.str().c_str()), atoi(os_month_.str().c_str()), atoi(os_day_.str().c_str()),
		atoi(os_hour_.str().c_str()), atoi(os_minute_.str().c_str()), atoi(os_second_.str().c_str()), atoi(os_millisecond_.str().c_str()));
	stmStr.assign(buf);

	return stmStr;
}


void PCI1761Trigger(string PCICardName)
{

	const char* char_path = "PCILogs/";
	if (_access(char_path, 0) == -1)
		_mkdir(char_path);

	ofstream pciLog;

	string picLogfileName = "";
	picLogfileName.append(char_path);
	picLogfileName.append("PICLog_");
	picLogfileName.append(getSysTime());
	picLogfileName.append(".txt");

	pciLog.open(picLogfileName);

#ifdef PCI

	char *CStr = const_cast<char *>(PCICardName.c_str());
	size_t len = strlen(CStr) + 1;
	size_t converted = 0;
	wchar_t *WStr;
	WStr = (wchar_t*)malloc(len * sizeof(wchar_t));
	mbstowcs_s(&converted, WStr, len, CStr, _TRUNCATE);

	pciLog << WStr << endl;

#endif

	int lastOpenTime = getUnixTime();
	int currentOpenTime = getUnixTime();


	for (;;)
	{
		if (!msgList.empty())
		{
			string message;

			mutexT.lock();
			message = msgList[0];
			msgList.erase(msgList.begin());
			mutexT.unlock();
			
			cout << getSysTime() << " message: " << message << endl;

			size_t fd = message.find("#");

			if (fd != message.npos){

				size_t fd1 = message.find("#", fd + 1);

				if (fd1 != message.npos){

					string strPCIPort = message.substr(fd + 1, fd1 - fd - 1);

					cout << getSysTime() << " PCI_port: " << strPCIPort << endl;

					int PCIPort = atoi(strPCIPort.c_str());
#ifdef PCI
					if (PCIPort >= 0 && PCIPort <= 7){

						currentOpenTime = getUnixTime();
						if ((currentOpenTime - lastOpenTime) > 1)
						{
							PCI1761* pPCI1761;
							pPCI1761 = new PCI1761(WStr);

							if (!pPCI1761->isInit()){
							pciLog << getSysTime() << " PCI-1761 class isn't init." << std::endl;
							cout << getSysTime() << " PCI-1761 class isn't init." << std::endl;
							return;
							}
							else{
							pciLog << getSysTime() << " PCI-1761 init success" << std::endl;
							cout << getSysTime() << " PCI-1761 init success" << std::endl;
							}

							if (!pPCI1761->OpenLED(PCIPort, 1000)){
								pciLog << getSysTime() << " LED open failed. port:" << PCIPort << std::endl;
								cout << getSysTime() << " LED open failed. port:" << PCIPort << std::endl;
							}
							else{
								Sleep(3000);
								pciLog << getSysTime() << " LED open successfully. port:" << PCIPort << std::endl;
								cout << getSysTime() << " LED open successfully. port:" << PCIPort << std::endl;
							}

							delete pPCI1761;
							lastOpenTime = currentOpenTime;
						}


					}
#endif
				}
			}
		}
		
	}

	pciLog.close();
}

int getUnixTime()
{
	stringstream sst;
	std::time_t time_result = std::time(nullptr);
	sst << time_result << endl;
	string unixTimeStr = sst.str();

	int currentUnixTime = atoi(unixTimeStr.c_str());

	return currentUnixTime;
}