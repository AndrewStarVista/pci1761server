#include <cstdlib>
#include <iostream>
#include <thread>
#include <utility>
#include <string>
#include <exception>  

#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "globalVariables.h"

#include "PCI1761Trigger.h"


using boost::asio::ip::tcp;
using namespace cv;
using namespace std;

typedef boost::shared_ptr<tcp::socket> socket_ptr;
const int LENGTH_BASE = 100000000;


void ServerThreadFunc(boost::asio::io_service& io_service, int port);
void HandleAccept(const boost::system::error_code& error,
	boost::shared_ptr< boost::asio::ip::tcp::socket > socket,
	boost::asio::ip::tcp::acceptor& acceptor);
void StartAccept(boost::asio::ip::tcp::acceptor& acceptor);
bool ParseImageData(vector<unsigned char> buf, size_t len, Mat& img);


boost::mutex mutexT;
vector<string> msgList;


void session(socket_ptr sock)
{
	try
	{
	    int max_length = 2048 * 2048 * 3;
		//int max_length = 100;
		std::vector<unsigned char> request_data(max_length);
		for (int t=0; t<1; t++)
		{
			size_t length;

			boost::system::error_code error;
			length = boost::asio::read(*sock, boost::asio::buffer(request_data, 9), error);

			if (error == boost::asio::error::eof)
				break; // Connection closed cleanly by peer.
			else if (error)
				throw boost::system::system_error(error); // Some other error.

			std::string data_len_str = std::string(request_data.begin(), request_data.begin() + length);
			int data_len = std::atoi(data_len_str.c_str()) - LENGTH_BASE;

			size_t req_length = boost::asio::read(*sock, boost::asio::buffer(request_data, data_len), error);

			if (error == boost::asio::error::eof)
				break; // Connection closed cleanly by peer.
			else if (error)
				throw boost::system::system_error(error); // Some other error.

			cout << "received " << endl;
			//// parse image data: 
			if (data_len > 1000){
				cout << "received data, length>1000 " << endl;
				Mat img;
				vector<unsigned char> img_data(request_data.begin(), request_data.begin() + data_len);

				bool b_check = ParseImageData(img_data, req_length, img);
			}
			else{
				string recvdMsg = std::string(request_data.begin(), request_data.begin() + req_length);
				cout << "received data, length<=1000,  content: " << recvdMsg << endl;

				mutexT.lock();
				if (msgList.size() >= 1)
				{
					msgList.pop_back();
				}
				msgList.push_back(recvdMsg);
				mutexT.unlock();

				string response = "true";

				int resp_len = response.length();

				std::string data = std::to_string(100000000 + resp_len) + response;
				int len_data = data.length();
			
				try{
					boost::asio::write(*sock, boost::asio::buffer(data, len_data));
				}
				catch (std::exception &e){
					cout << e.what() << endl;
				}
			}
		}
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception in thread: " << e.what() << "\n";
	}
}

void server(boost::asio::io_service& io_service, unsigned short port)
{
	tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));
	for (;;)
	{
		socket_ptr sock(new tcp::socket(io_service));
		a.accept(*sock);
		boost::thread(session, std::move(sock)).detach();
	}
}


void threadTCPMsgReceive(int& targetPort)
{
	using boost::asio::ip::tcp;

	// Create io service.


	for (;;){

		boost::asio::io_service io_service;

		// Create server thread that will start accepting connections.
		cout << "-------------------------------create server -------------------------" << endl;
		boost::thread server_thread(ServerThreadFunc, boost::ref(io_service), targetPort);

		// Sleep for 10 seconds, then shutdown the server.
		std::cout << "Stopping service in 60 seconds..." << std::endl;
		boost::this_thread::sleep(boost::posix_time::seconds(60));
		std::cout << "Stopping service now!" << std::endl;

		// Stopping the io_service is a non-blocking call.  The threads that are
		// blocked on io_service::run() will try to return as soon as possible, but
		// they may still be in the middle of a handler.  Thus, perform a join on 
		// the server thread to guarantee a block occurs.
		io_service.stop();

		std::cout << "Waiting on server thread..." << std::endl;
		server_thread.join();
		std::cout << "Done waiting on server thread." << std::endl;
	}


	return;

}

void ServerThreadFunc(boost::asio::io_service& io_service, int port)
{
	using boost::asio::ip::tcp;
	tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), port));

	// Add a job to start accepting connections.
	StartAccept(acceptor);

	// Process event loop.
	io_service.run();

	std::cout << "Server thread exiting." << std::endl;
}

void HandleAccept(const boost::system::error_code& error,
	boost::shared_ptr< boost::asio::ip::tcp::socket > socket,
	boost::asio::ip::tcp::acceptor& acceptor)
{
	// If there was an error, then do not add any more jobs to the service.
	if (error)
	{
		std::cout << "Error accepting connection: " << error.message()
			<< std::endl;
		return;
	}

	// Otherwise, the socket is good to use.
	std::cout << "Doing things with socket..." << std::endl;

	boost::thread(session, std::move(socket)).detach();

	// Perform async operations on the socket.

	// Done using the socket, so start accepting another connection.  This
	// will add a job to the service, preventing io_service::run() from
	// returning.
	std::cout << "Done using socket, ready for another connection."
		<< std::endl;
	StartAccept(acceptor);
};

void StartAccept(boost::asio::ip::tcp::acceptor& acceptor)
{
	using boost::asio::ip::tcp;
	boost::shared_ptr< tcp::socket > socket(
		new tcp::socket(acceptor.get_io_service()));

	// Add an accept call to the service.  This will prevent io_service::run()
	// from returning.
	std::cout << "Waiting on connection" << std::endl;
	acceptor.async_accept(*socket,
		boost::bind(HandleAccept,
		boost::asio::placeholders::error,
		socket,
		boost::ref(acceptor)));
}




bool  ParseImageData(vector<unsigned char> buf, size_t len, Mat& img)
{
	if (len > 0)
	{
		std::vector<unsigned char> headVec(buf.begin()+1, buf.begin()+1 + 11);
		std::vector<unsigned char> endVec(buf.begin()+ len - 4, buf.begin()+1 + len);

		string endStr(endVec.begin(), endVec.end());

//		cout << "endStr: " << endStr << endl;

		if (strcmp(endStr.c_str(), "####") != 0)
		{
			cout << "wrong img data" << endl;
			return false;
		}
		else
		{
			string headStr(headVec.begin(), headVec.end());
			cout << "headStr: " << headStr << endl;
			size_t fd1;
			fd1 = headStr.find("S");
			if (fd1 != headStr.npos)
			{
				size_t fd2 = headStr.find("X");
				string strWid = headStr.substr(fd1 + 1, 4);
				string strHgt = headStr.substr(fd2 + 1, 4);


				int imgWid = atoi(strWid.c_str());
				int imgHgt = atoi(strHgt.c_str());

				cout << "wid: " << imgWid << " Hei:" << imgHgt << endl;

				int imgDataSize = imgWid* imgHgt * 3;

				std::vector<unsigned char> imgData(buf.begin() + 11+1, buf.begin() + len - 4);

				cout << "Received imgDataSize: " << imgData.size() << endl;
				if (imgData.size() == imgDataSize)
				{
					cv::Mat data_mat(imgData, true);
				    img = data_mat.reshape(3, imgHgt);

					imshow("SERVER Received", img);
					waitKey(10);
				}
				else
				{
					cout << "img data size error" << endl;
					return false;
				}

			}
			else
			{
				return true;
			}
		}
	}

}

int main(int argc, char* argv[])
{
	int port;
	string PCICardName;

	try
	{

		for (int i = 1; i < argc; ++i)
		{
			if (!strcmp(argv[i], "-port") || !strcmp(argv[i], "-PORT"))
			{
				port = atoi(argv[++i]);
			}

			if (!strcmp(argv[i], "-cardname") || !strcmp(argv[i], "-CARDNAME"))
			{
				PCICardName = argv[++i];
			}
		}

		if (port < 1025) {
			std::cerr << "[ERROR] PORT can not be less than 1025. " << "Port:" << port << std::endl;

			system("pause");
			exit(1);
		}

		boost::thread rcvMsgThread(threadTCPMsgReceive, port);

	//	boost::asio::io_service io_service;
		PCI1761Trigger(PCICardName);
	//	boost::thread PCI1761Thread(PCI1761Trigger, PCICardName);
	//	server(io_service, port);
		rcvMsgThread.join();
	//	PCI1761Thread.join();
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}
