#include "PCI1761.h"
#include <stdio.h>
#include <math.h>

PCI1761::PCI1761()
{
	m_instantDoCtrl = InstantDoCtrl::Create();
	m_isInit = false;
	m_hThread = nullptr;
	m_port = 0;
	m_dwMilliseconds = 1000;
	m_isEnd = false;
	m_isSuccess = false;
	m_state = 0;
}

PCI1761::PCI1761(const wchar_t *deviceDesc)
{
	m_instantDoCtrl = InstantDoCtrl::Create();
	m_isInit = false;
	m_hThread = nullptr;
	m_port = 0;
	m_dwMilliseconds = 1000;
	m_isEnd = false;
	m_isSuccess = false;
	m_state = 0;
	Init_(deviceDesc);
}

PCI1761::~PCI1761()
{
	if (m_hThread)
	{
		if (WaitForSingleObject(m_hThread, 5) != WAIT_OBJECT_0)
		{
			TerminateThread(m_hThread, 0);
		}
		CloseHandle(m_hThread);
		m_hThread = nullptr;
	}

	m_instantDoCtrl->Dispose();
}

bool PCI1761::Init(const wchar_t *deviceDesc)
{
	Init_(deviceDesc);
	return m_isInit;
}

void PCI1761::Init_(const wchar_t *deviceDesc)
{
	ErrorCode  ret = Success;
	DeviceInformation devInfo(deviceDesc, ModeWrite, 0);
	ret = m_instantDoCtrl->setSelectedDevice(devInfo);
	if (BioFailed(ret))
	{
		m_isInit = false;
		printf(" 0.Some error occurred. And the last error code is 0x%X.\n", ret);
		return;
	}

	m_isInit = true;
}

bool PCI1761::OpenLED(unsigned char port, unsigned long dwMilliseconds)
{
	m_port = port;
	m_dwMilliseconds = dwMilliseconds;
	m_isEnd = false;
	m_isSuccess = false;
	if (m_hThread)
	{
		if (WaitForSingleObject(m_hThread, 5) != WAIT_OBJECT_0)
		{
			TerminateThread(m_hThread, 0);
		}
		CloseHandle(m_hThread);
		m_hThread = nullptr;
	}
	m_hThread = CreateThread(nullptr, 0, ProcessThread_, this, 0, nullptr);

	while (true)
	{
		if (m_isEnd)
		{
			break;
		}
		Sleep(1);
	}
	if (m_isSuccess)
		return true;
	else
		return false;
}

unsigned char PCI1761::GetStatus(unsigned char port)
{
	port = pow(2.0, port);
	ErrorCode  ret = Success;
	ret = m_instantDoCtrl->Read(0, m_state);
	if (BioFailed(ret))
	{
		printf(" GetStatus(%d).Some error occurred. And the last error code is 0x%X.\n", port, ret);
		return 100;
	}
	return (m_state & port);
}

DWORD WINAPI PCI1761::ProcessThread_(LPVOID lpParam)
{
	PCI1761 *pPCI1761 = (PCI1761 *)lpParam;
	unsigned char tmport = pow(2.0, pPCI1761->m_port);
	int dwMilseconds = pPCI1761->m_dwMilliseconds;
	if (tmport == pPCI1761->GetStatus(pPCI1761->m_port))
	{
		pPCI1761->m_isSuccess = true;
		pPCI1761->m_isEnd = true;		
		Sleep(pPCI1761->m_dwMilliseconds);		
		pPCI1761->m_instantDoCtrl->Write(0, (pPCI1761->m_state^tmport));
		Sleep(15);
		printf("1. port %d status is:  0x%X\n", pPCI1761->m_port, pPCI1761->GetStatus(pPCI1761->m_port)); //very important,  update the pPCI1761->m_state value and cannot delete this line.
		return 0;
	}

	ErrorCode  ret = Success;

	ret = pPCI1761->m_instantDoCtrl->Write(0, (pPCI1761->m_state | tmport));
	Sleep(15);
	printf("2. port %d status is:  0x%X\n", pPCI1761->m_port, pPCI1761->GetStatus(pPCI1761->m_port)); //very important,  update the pPCI1761->m_state value and cannot delete this line.
	if (BioFailed(ret))
	{
		pPCI1761->m_isSuccess = false;
		pPCI1761->m_isEnd = true;
		printf(" Some error occurred. And the last error code is 0x%X.\n", ret);
		return -1;
	}
	pPCI1761->m_isSuccess = true;
	pPCI1761->m_isEnd = true;
	Sleep(dwMilseconds);
	pPCI1761->m_instantDoCtrl->Write(0, (pPCI1761->m_state^tmport));
	Sleep(15);
	printf("3. port %d status is:  0x%X\n", pPCI1761->m_port, pPCI1761->GetStatus(pPCI1761->m_port)); //very important,  update the pPCI1761->m_state value and cannot delete this line.
	return 0;	
}

