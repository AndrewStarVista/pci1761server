#ifndef PCI_1761_H__
#define PCI_1761_H__

#include "compatibility.h"
#include "bdaqctrl.h"
using namespace Automation::BDaq;

class PCI1761
{
public:
	PCI1761();
	PCI1761(const wchar_t *deviceDesc);
	~PCI1761();

public:
	bool Init(const wchar_t *deviceDesc);
	bool OpenLED(unsigned char port, unsigned long dwMilliseconds);
	unsigned char GetStatus(unsigned char port);  // if error, return 100;
	inline bool isInit()
	{
		return m_isInit;
	}

private:
	static DWORD WINAPI ProcessThread_(LPVOID lpPara);
	void Init_(const wchar_t *deviceDesc);

private:
	InstantDoCtrl * m_instantDoCtrl;
	HANDLE m_hThread;
	bool m_isInit;
	unsigned char m_port;
	unsigned long m_dwMilliseconds;
	bool m_isEnd;
	bool m_isSuccess;
	unsigned char m_state;
};


#endif // PCI_1761_H__
