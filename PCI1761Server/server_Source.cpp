#include <cstdlib>
#include <iostream>
#include <thread>
#include <utility>
#include <string>
#include <exception>  

#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using boost::asio::ip::tcp;
using namespace cv;
using namespace std;

typedef boost::shared_ptr<tcp::socket> socket_ptr;
const int LENGTH_BASE = 100000000;

bool  ParseImageData(vector<unsigned char> buf, size_t len, Mat& img);

void session(socket_ptr sock)
{
	try
	{
	    int max_length = 2048 * 2048 * 3;
		//int max_length = 100;
		std::vector<unsigned char> request_data(max_length);
		for (;;)
		{
			size_t length;

			boost::system::error_code error;
			length = boost::asio::read(*sock, boost::asio::buffer(request_data, 9), error);

			if (error == boost::asio::error::eof)
				break; // Connection closed cleanly by peer.
			else if (error)
				throw boost::system::system_error(error); // Some other error.

			std::string data_len_str = std::string(request_data.begin(), request_data.begin() + length);
			int data_len = std::atoi(data_len_str.c_str()) - LENGTH_BASE;

			size_t req_length = boost::asio::read(*sock, boost::asio::buffer(request_data, data_len), error);

			if (error == boost::asio::error::eof)
				break; // Connection closed cleanly by peer.
			else if (error)
				throw boost::system::system_error(error); // Some other error.

			cout << "received " << endl;
			//// parse image data: 
			if (data_len > 1000){
				cout << "received data, length>1000, should be image" << endl;
				Mat img;
				vector<unsigned char> img_data(request_data.begin(), request_data.begin() + data_len);

				bool b_check = ParseImageData(img_data, req_length, img);
			}
			else{
				cout << "received data, length<=1000, should be message, content: " << std::string(request_data.begin(), request_data.begin() + req_length) << endl;

				string response = "true";

				int resp_len = response.length();

				std::string data = std::to_string(100000000 + resp_len) + response;
				int len_data = data.length();

			
				try{
					boost::asio::write(*sock, boost::asio::buffer(data, len_data));
				}
				catch (std::exception &e){
					cout << e.what() << endl;
				}
			}
		}
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception in thread: " << e.what() << "\n";
	}
}

void server(boost::asio::io_service& io_service, unsigned short port)
{
	tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));
	for (;;)
	{
		socket_ptr sock(new tcp::socket(io_service));
		a.accept(*sock);
		boost::thread(session, std::move(sock)).detach();
	}
}




bool  ParseImageData(vector<unsigned char> buf, size_t len, Mat& img)
{
	if (len > 0)
	{
		std::vector<unsigned char> headVec(buf.begin()+1, buf.begin()+1 + 11);
		std::vector<unsigned char> endVec(buf.begin()+ len - 4, buf.begin()+1 + len);

		string endStr(endVec.begin(), endVec.end());

//		cout << "endStr: " << endStr << endl;

		if (strcmp(endStr.c_str(), "####") != 0)
		{
			cout << "wrong img data" << endl;
			return false;
		}
		else
		{
			string headStr(headVec.begin(), headVec.end());
			cout << "headStr: " << headStr << endl;
			size_t fd1;
			fd1 = headStr.find("S");
			if (fd1 != headStr.npos)
			{
				size_t fd2 = headStr.find("X");
				string strWid = headStr.substr(fd1 + 1, 4);
				string strHgt = headStr.substr(fd2 + 1, 4);


				int imgWid = atoi(strWid.c_str());
				int imgHgt = atoi(strHgt.c_str());

				cout << "wid: " << imgWid << " Hei:" << imgHgt << endl;

				int imgDataSize = imgWid* imgHgt * 3;

				std::vector<unsigned char> imgData(buf.begin() + 11+1, buf.begin() + len - 4);

				cout << "Received imgDataSize: " << imgData.size() << endl;
				if (imgData.size() == imgDataSize)
				{
					cv::Mat data_mat(imgData, true);
				    img = data_mat.reshape(3, imgHgt);

					imshow("SERVER Received", img);
					waitKey(10);
				}
				else
				{
					cout << "img data size error" << endl;
					return false;
				}

			}
			else
			{
				return true;
			}
		}
	}

}

int main(int argc, char* argv[])
{
	int port;
	try
	{

		for (int i = 1; i < argc; ++i)
		{
			if (!strcmp(argv[i], "-port") || !strcmp(argv[i], "-PORT"))
			{
				port = atoi(argv[++i]);
			}
		}

		if (port < 1025) {
			std::cerr << "[ERROR] PORT can not be less than 1025. " << "Port:" << port << std::endl;

			system("pause");
			exit(1);
		}


		boost::asio::io_service io_service;

		server(io_service, port);
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}
