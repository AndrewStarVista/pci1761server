#include <iostream>
#include <fstream>
#include <io.h>
#include <direct.h>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include "PCI/PCI1761.h"

//#define PCI


PCI1761* pPCI1761;



using boost::asio::ip::tcp;
using namespace std;

string getSysTime();

#ifdef PCI
int PCIInitialize(PCI1761* PCI1761_ptr, string PCICardName);
int sendAlarm(PCI1761* PCI1761_ptr, int PCICardPort, int PCILightDuration);
#endif

ofstream pciLog;

int main(int argc, char* argv[])
{

	if (argc != 4)
	{
		cout << "usage: PCIReceiver.exe receiverPort PCICardName PCILightDuration" << endl;
		return 0;
	}


	const char* char_path = "PCILogs/";
	if (_access(char_path, 0) == -1)
		_mkdir(char_path);

	int tcpPort = atoi(argv[1]);
	string PCICardName = argv[2];
	int PCILightDuration = atoi(argv[3]);


#ifdef PCI
	string picLogfileName = "";
	picLogfileName.append(char_path);
	picLogfileName.append("PICLog_");
	picLogfileName.append(getSysTime());
	picLogfileName.append(".txt");

	pciLog.open(picLogfileName);
	
	//PCIInitialize(pPCI1761, PCICardName);

	char *CStr = const_cast<char *>(PCICardName.c_str());
	size_t len = strlen(CStr) + 1;
	size_t converted = 0;
	wchar_t *WStr;
	WStr = (wchar_t*)malloc(len * sizeof(wchar_t));
	mbstowcs_s(&converted, WStr, len, CStr, _TRUNCATE);

	pciLog << WStr << endl;

	pPCI1761 = new PCI1761(WStr);
	if (!pPCI1761->isInit())
	{
		pciLog << "PCI-1761 class isn't init." << std::endl;
		
	}
	else
	{
		pciLog << "PCI-1761 init success" << std::endl;
		
	}


#endif

	
	while (1)
	{
		try
		{
			boost::asio::io_service io_service;
			tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), tcpPort));

			for (;;)
			{
				tcp::socket socket(io_service);
				acceptor.accept(socket);


				boost::array<char, 128> buf;
				boost::system::error_code error;

				size_t len = socket.read_some(boost::asio::buffer(buf), error);
				if (error == boost::asio::error::eof)
					break;  
				else if (error)
					throw boost::system::system_error(error);  

				string message = buf.data();
				cout << getSysTime() <<"    message: " << message << endl;

				size_t fd = message.find("#");
				size_t fd1 = message.find("#", fd + 1);
				string strPCIPort = message.substr(fd+1, fd1-fd-1);

				cout << getSysTime() << "   PCI_port: " << strPCIPort << endl;
 
				int PCIPort = atoi(strPCIPort.c_str());

#ifdef PCI
				//sendAlarm(pPCI1761, PCIPort, PCILightDuration);
				if (!pPCI1761->OpenLED(PCIPort, PCILightDuration))
				{
					pciLog << getSysTime() << ": LED open failed. port:" << PCIPort << std::endl;
				//	return 0;
				}
				else
				{
					pciLog << getSysTime() << ":LED open successfully. port:" << PCIPort << std::endl;
				//	return 1;
				}

#endif

		
			}
		}
		catch (std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}
	

	return 0;
}


int sendAlarm(PCI1761* PCI1761_ptr, int PCICardPort, int PCILightDuration)
{

	if (!PCI1761_ptr->OpenLED(PCICardPort, PCILightDuration))
	{
		pciLog << getSysTime()<< ": LED open failed. port:"<< PCICardPort  << std::endl;
		return 0;
	}
	else
	{
		pciLog << getSysTime()<< ":LED open successfully. port:" << PCICardPort << std::endl;
		return 1;
	}

}

#ifdef PCI
int PCIInitialize(PCI1761* PCI1761_ptr, string PCICardName)
{
	//const wchar_t *deviceDesc = L"firstCard";

	// Convert to a wchar_t*
	char *CStr = const_cast<char *>(PCICardName.c_str());
	size_t len = strlen(CStr) + 1;
	size_t converted = 0;
	wchar_t *WStr;
	WStr = (wchar_t*)malloc(len * sizeof(wchar_t));
	mbstowcs_s(&converted, WStr, len, CStr, _TRUNCATE);

	pciLog << WStr << endl;

	PCI1761_ptr = new PCI1761(WStr);
	if (!PCI1761_ptr->isInit())
	{
		pciLog << "PCI-1761 class isn't init." << std::endl;
		return 0;
	}
	else
	{
		pciLog << "PCI-1761 init success" << std::endl;
		return 1;
	}
}
#endif

string getSysTime()
{
	SYSTEMTIME sys;
	GetLocalTime(&sys);
	std::stringstream ss;

	stringstream os_year_, os_month_, os_day_, os_hour_, os_minute_, os_second_, os_millisecond_;
	os_year_ << sys.wYear;
	os_month_ << sys.wMonth;
	os_day_ << sys.wDay;
	os_hour_ << sys.wHour;
	os_minute_ << sys.wMinute;
	os_second_ << sys.wSecond;
	os_millisecond_ << sys.wMilliseconds;

	string stmStr;
	char buf[1024];
	sprintf(buf, "%04d%02d%02d%02d%02d%02d%03d", atoi(os_year_.str().c_str()), atoi(os_month_.str().c_str()), atoi(os_day_.str().c_str()),
		atoi(os_hour_.str().c_str()), atoi(os_minute_.str().c_str()), atoi(os_second_.str().c_str()), atoi(os_millisecond_.str().c_str()));
	stmStr.assign(buf);

	return stmStr;
}