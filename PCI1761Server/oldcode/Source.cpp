#include <iostream>
#include <thread>

#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <string>
#include <fstream>
#include <time.h>



using boost::asio::ip::tcp;
typedef boost::shared_ptr<tcp::socket> socket_ptr;
using namespace cv;
using namespace std;

struct tm newtime;
__time32_t aclock;

const long LENGTH_BASE = 100000000;
const int  LENGTH_SIZE = 9;

void Log(std::string& text)
{
	//std::string time_str;

	char buffer[32];
	errno_t errNum;
	_time32(&aclock);   // Get time in seconds.
	_localtime32_s(&newtime, &aclock);   // Convert time to struct tm form.

	// Print local time as a string.

	errNum = asctime_s(buffer, 32, &newtime);
	if (errNum)
	{
		printf("current time Error code: %d", (int)errNum);
		return;
	}

	printf("Time:%s%s \n\n", buffer, text.data());
}

bool  ParseImageData(vector<unsigned char> buf, size_t len, Mat& img)
{
	if (len > 0)
	{
		std::vector<unsigned char> headVec(buf.begin() + 1, buf.begin() + 1 + 11);
		std::vector<unsigned char> endVec(buf.begin() + len - 4, buf.begin() + 1 + len);

		string endStr(endVec.begin(), endVec.end());

		cout << "endStr: " << endStr << endl;

		if (strcmp(endStr.c_str(), "####") != 0)
		{
			cout << "wrong img data" << endl;
			return false;
		}
		else
		{
			string headStr(headVec.begin(), headVec.end());
			cout << "headStr: " << headStr << endl;
			size_t fd1;
			fd1 = headStr.find("S");
			if (fd1 != headStr.npos)
			{
				size_t fd2 = headStr.find("X");
				string strWid = headStr.substr(fd1 + 1, 4);
				string strHgt = headStr.substr(fd2 + 1, 4);


				int imgWid = atoi(strWid.c_str());
				int imgHgt = atoi(strHgt.c_str());

				cout << "wid: " << imgWid << " Hei:" << imgHgt << endl;

				int imgDataSize = imgWid* imgHgt * 3;

				std::vector<unsigned char> imgData(buf.begin() + 11 + 1, buf.begin() + len - 4);

				cout << "Received imgDataSize: " << imgData.size() << endl;
				if (imgData.size() == imgDataSize)
				{
					cv::Mat data_mat(imgData, true);
					img = data_mat.reshape(3, imgHgt);

					imshow("img", img);
					waitKey(1);
				}
				else
				{
					cout << "img data size error" << endl;
					return false;
				}

			}
			else
			{
				return true;
			}
		}
	}

}

//void sendResponse(socket_ptr socket, TransImageJ::Response& response)
//{
//
//	std::string res_data;
//	int res_len = response.ByteSize();
//
//	if (!response.SerializeToString(&res_data)) {
//		std::cerr << "SerializeToString error!" << " Request id: " << response.request_id() << std::endl;
//		return;
//	}
//
//	std::string data = std::to_string(LENGTH_BASE + res_len) + res_data;
//	int len_data = data.length();
//
//	//std::cout << "byte size:" << res_len << " final data size:" << len_data << "data:" << data << std::endl;
//
//	try {
//		boost::asio::write(*socket, boost::asio::buffer(data, data.length()));
//	}
//	catch (std::exception& e) {
//		std::cerr << "send response exception!" << e.what() << " Request id: " << response.request_id() << std::endl;
//	}
//}


void session(socket_ptr socket) {

	int _total_request = 0;

	try {

		std::cout << "New Session " << std::endl;
		int max_length = 2048 * 2048 * 3;
		std::vector<char> request_data(max_length);

		for (;;) {

			size_t data_size;
			size_t req_length;

			boost::system::error_code error;
			data_size = boost::asio::read(*socket, boost::asio::buffer(request_data, LENGTH_SIZE), error);

			if (error == boost::asio::error::eof) {
				std::cout << "Connection closed cleanly by peer." << std::endl;
				break;
			}
			else if (data_size != LENGTH_SIZE) {
				std::cout << "Length_size: " << data_size << "!=" << LENGTH_SIZE << std::endl;
				break;
			}
			else if (error) {
				throw new boost::system::system_error(error);
			}

			//std::cout << "request data size:" << data_size << std::endl;
			std::string data_len_str = std::string(request_data.begin(), request_data.begin() + data_size);
			int data_len = std::atoi(data_len_str.c_str()) - LENGTH_BASE;

			req_length = boost::asio::read(*socket, boost::asio::buffer(request_data, data_len), error);

			if (error == boost::asio::error::eof) {
				std::cout << "Connection closed cleanly by peer." << std::endl;
				break; // Connection closed cleanly by peer.
			}
			else if (req_length != data_len) {
				std::cout << "Request length: " << req_length << "!=" << data_len << std::endl;
				break;
			}
			else if (error)
				throw boost::system::system_error(error); // Some other error.


			Mat img;
			vector<unsigned char> img_data(request_data.begin(), request_data.begin() + data_len);

			bool b_check = ParseImageData(img_data, req_length, img);
			//TransImageJ::Response response;

			//StoreData(std::string(request_data.begin(), request_data.begin() + req_length), response);

			//sendResponse(socket, response);

			//_total_request++;

			//currentTime();
			Log("New request: " + std::to_string(_total_request));
		}
	}
	catch (std::exception& e) {
		std::cout << "Socket error! " << e.what() << std::endl;
	}
}



void server(boost::asio::io_service& io_service, unsigned short port)
{
	tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), port));
	std::cout << "Server is running! port:" << port << std::endl;

	for (;;) {
		socket_ptr socket(new tcp::socket(io_service));
		acceptor.accept(*socket);
		boost::thread(session, std::move(socket)).detach();
	}

}

int main(int argc, char* argv[]) {

	int port = 0;
	std::string dbip;
	std::string dbport;

	for (int i = 1; i < argc; ++i)
	{
		if (!strcmp(argv[i], "-port") || !strcmp(argv[i], "-PORT"))
		{
			port = atoi(argv[++i]);
		}
		/*else if (!strcmp(argv[i], "-dbip") || !strcmp(argv[i], "-DBIP"))
		{
			dbip = argv[++i];
		}
		else if (!strcmp(argv[i], "-dbport") || !strcmp(argv[i], "-DBPORT"))
		{
			dbport = argv[++i];
		}*/

	}

	if (port < 1025) {
		std::cerr << "[ERROR] PORT can not be less than 1025. " << "Port:" << port << std::endl;

		system("pause");
		exit(1);
	}
	/*if (dbip.empty() || dbport.empty()) {
		std::cerr << "[ERROR] DB ip or port can not empty. " << std::endl;
		std::cout << "Usage: -dbip [ip] -dbport [port]" << std::endl;

		system("pause");
		exit(1);
	}

	std::cout << "Server port: " << port << " Dbip:" << dbip << " Dbport:" << dbport << std::endl;*/


	//std::thread  _consumeTh(ConsumeData, dbip + ":" + dbport);

	boost::asio::io_service io_service;
	server(io_service, port);

	//_consumeTh.join();
}