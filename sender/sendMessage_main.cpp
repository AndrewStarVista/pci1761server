#include <ctime>
#include <iostream>
#include <string>
#include <boost/asio.hpp>

using namespace boost;
using boost::asio::ip::tcp;
using namespace std;

int main(int argc, char* argv[])
{
	try
	{
		if (argc != 3)
		{
			cout << "usage: sendMessage.exe IP Port" << endl;
			std::system("pause");
			return 0;
		}
			
		asio::io_service io_service;
		string targetIP = argv[1];
		int targetPort = atoi(argv[2]);
	    tcp::endpoint end_point(boost::asio::ip::address::from_string(targetIP), targetPort);

		for (;;)
		{
			try
			{
				cout << "pls input command:" << endl;
				std::string message;//argv[3];
				cin >> message;

				tcp::socket socket(io_service);

				socket.connect(end_point);


				int req_len = message.length();

				int totalLen = 100000000 + req_len;



				std::string data = std::to_string(100000000 + req_len) + message;
				int len_data = data.length();

				system::error_code ignored_error;
				socket.write_some(asio::buffer(data), ignored_error);
			}
			catch(std::exception& e)
			{
				std::cerr << e.what() << std::endl;
			}
			
		}
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}

	return 0;
}