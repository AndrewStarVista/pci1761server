#include <ctime>
#include <iostream>
#include <string>
#include <boost/asio.hpp>

using namespace boost;
using boost::asio::ip::tcp;
using namespace std;

int main(int argc, char* argv[])
{
	try
	{
		asio::io_service io_service;
	//	tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), 3200));
		string targetIP = argv[1];
		int targetPort = atoi(argv[2]);
	//	tcp::endpoint end_point(boost::asio::ip::address::from_string(targetIP), targetPort);
		tcp::endpoint end_point(boost::asio::ip::address::from_string(targetIP), targetPort);
		for (;;)
		{
			try
			{
				tcp::socket socket(io_service);
				//	acceptor.accept(socket);
				socket.connect(end_point);
				time_t now = time(0);
				std::string message = ctime(&now);

				system::error_code ignored_error;
				socket.write_some(asio::buffer(message), ignored_error);
			}
			catch(std::exception& e)
			{
				std::cerr << e.what() << std::endl;
			}
			
		}
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}

	return 0;
}