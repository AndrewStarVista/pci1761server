#include <ctime>
#include <iostream>
#include <string>
#include <boost/asio.hpp>

using namespace boost;
using boost::asio::ip::tcp;
using namespace std;

int main(int argc, char* argv[])
{
	try
	{
		asio::io_service io_service;
		string targetIP = argv[1];
		int targetPort = atoi(argv[2]);
	    tcp::endpoint end_point(boost::asio::ip::address::from_string(targetIP), targetPort);

		for (;;)
		{
			try
			{
				cout << "pls input command:" << endl;
				std::string message;//argv[3];
				cin >> message;

				tcp::socket socket(io_service);

				socket.connect(end_point);


				system::error_code ignored_error;
				socket.write_some(asio::buffer(message), ignored_error);
			}
			catch(std::exception& e)
			{
				std::cerr << e.what() << std::endl;
			}
			
		}
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}

	return 0;
}